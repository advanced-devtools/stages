#!/bin/bash

# 
# Example script
# 
# Author Vit Kabele <vit@kabele.me>
#

if [ -z "$1" ];then
    echo "Please provide one parameter" && exit 1;
fi

echo "Running test $1";
