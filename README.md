# Stages

In this example we will se how to use multiple stages and some before and after
scripts.

## Notes

* The jobs in one stage run in parallel
* Default stages are
    * build
    * test
    * deploy
* The created file does not stay for next jobs
* Local before script overrides global ones

